import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Product } from '../../Product.model';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  @Output() ProductWasSelected = new EventEmitter<Product>();
  @ViewChild('name') nameRef: ElementRef;
  @ViewChild('desc') descRef: ElementRef;
  @ViewChild('price') priceRef: ElementRef;
  @ViewChild('f') addingForm: NgForm;

  addForm = false;
  NameCheck = '';
  DescCheck = '';
  PriceCheck: number = null;
  nameEx = false;
  @Input() products_mas: Product[];
  constructor() { }

  ngOnInit() {
  }

  onProductSelected(product: Product) {
    this.ProductWasSelected.emit(product);
  }
  onAdd() {
    this.addForm = true;
  }
  onHide() {
    this.addForm = false;
  }
  onAddItem() {
    const name = this.nameRef.nativeElement.value;
    const desc = this.descRef.nativeElement.value;
    const price = this.priceRef.nativeElement.value;
    for (let i = 0; i < this.products_mas.length; i++) {
      if (name === this.products_mas[i].name) {
        this.nameEx = true;
        break;
     }
    }
    if (this.nameEx === false) {
      if (this.products_mas.length === 0) {
        const newProduct = new Product(name, desc, 0, price);
        this.products_mas.push(newProduct);
        } else {
         const newProduct = new Product(name, desc, this.products_mas.length, price);
          this.products_mas.push(newProduct);
        }
    }
    this.nameEx = false;
    this.addingForm.reset();
  }
   onSubmit () {
    console.log(this.addingForm);
   }
}
