import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../../../Product.model';



@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {

  @Input() product: Product;
  @Output() ProductSelected = new EventEmitter<void>();
  constructor() { }

  ngOnInit() {
  }

  onSelected() {
    this.ProductSelected.emit();
  }
  checker() {   // вот это отжёг с переносами конечно
    if ((this.product.name === ''
    || this.product.name === undefined)
    || (this.product.description === ''
    || this.product.description === undefined)) {
      return false;
    } else {
      return true; }
  }
}
