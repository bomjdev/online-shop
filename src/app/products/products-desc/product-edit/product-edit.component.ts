import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { Product } from '../../../Product.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  @Input() product: Product;
  @Input() openEdit;
  @ViewChild('name') nameRef: ElementRef;
  @ViewChild('price') priceRef: ElementRef;
  @ViewChild('desc') descRef: ElementRef;
  @ViewChild('editForm') editRef: NgForm;
  @Output() onEdit = new EventEmitter<Product>();

  constructor() { }

  ngOnInit() {
  }

  onSubmit() {
    const name = this.nameRef.nativeElement.value;
    const price = this.priceRef.nativeElement.value;
    const desc = this.descRef.nativeElement.value;
    const index = this.product.index;
    const editedItem = new Product(name, desc, index, price);
    this.onEdit.emit(editedItem);
  }

  // onHide() {
  //   this.openEdit = false;        !!!!!!!!!!!!!!!!!!!!!!!!!!!
  // }
}
