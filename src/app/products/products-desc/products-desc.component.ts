import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Product } from '../../Product.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-products-desc',
  templateUrl: './products-desc.component.html',
  styleUrls: ['./products-desc.component.css']
})
export class ProductsDescComponent implements OnInit {

  @Input() product: Product;
  @Output() onDeleted = new EventEmitter<number>();
  @Output() AddToCartClicked = new EventEmitter<void>();
  @Output() onEdit = new EventEmitter<Product>();
  @ViewChild('amForm') amountForm: NgForm;
  @ViewChild('amount') amount: ElementRef;
  opened = false;
  @Output() openEdit = false;

  constructor() { }

  ngOnInit() {
  }

  onClickAddToCart() {
    this.product.amount = this.amount.nativeElement.value;
    this.AddToCartClicked.emit();
     this.amountForm.form.reset({
       amount: '1'
     });
  }
  onDelete() {
    this.onDeleted.emit(this.product.index);
  }

  clickAdd() {
    this.opened = true;
  }

  clickHide() {
    this.opened = false;
  }

  onClickEdit(product: Product) {
    this.onEdit.emit(product);
  }

  onShowEdit() {
    this.openEdit = !this.openEdit;
  }
}
