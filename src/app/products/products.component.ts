import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Product } from '../Product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  @Output() productAdded = new EventEmitter<Product>();
  @Output() SelectedProduct: Product;
  @Output() onEdited = new EventEmitter<Product>();
  @Input() xxx: Product[];
  @Output() ONdeleted = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  productSelected(product: Product) {
    this.SelectedProduct = product;
    return product;
}
  AddToCartClicked() {
    const item = this.SelectedProduct;
    this.productAdded.emit(item);
  }
  onDeleted(index: number) {
    this.ONdeleted.emit(index);
    this.SelectedProduct = null;
  }

  onEdit(product: Product) {
    this.onEdited.emit(product);
  }
}
