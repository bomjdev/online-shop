import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { ProductsComponent } from './products/products.component';
import { ProductListComponent } from './Products/product-list/product-list.component';
import { ProductItemComponent } from './products/product-list/product-item/product-item.component';
import { ProductsDescComponent } from './products/products-desc/products-desc.component';
import { ShoppingComponent } from './shopping/shopping.component';
import { DropdownDirective } from './shared/dropdown.directive';
import { ProductEditComponent } from './products/products-desc/product-edit/product-edit.component';

// const appRoutes: Routes = [
//   { path: '', component: ProductsComponent},
//   { path: 'cart', component: ShoppingComponent}
// ];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProductsComponent,
    ProductListComponent,
    ProductItemComponent,
    ProductsDescComponent,
    ShoppingComponent,
    DropdownDirective,
    ProductEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    // RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
