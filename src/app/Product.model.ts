export class Product {
    public name: string;
    public description: string;
    public imagePath: string;
    public index: number;
    public price: number;
    public amount: number;

    constructor (name: string, desc: string, index: number, price: number) {
        this.name =  name;
        this.description = desc;
        this.index = index;
        this.price = price;
    }
}
