import { Component, Output, EventEmitter } from '@angular/core';
import { Product } from './Product.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @Output() addedProduct = new EventEmitter<Product>();

  temp: Product = new Product( 'Лук' , 'Просто лук', 0 , 20);
  temp1: Product = new Product('Укроп', 'Очень востребован', 1, 10);
  temp2: Product = new Product('Помидор', 'красный', 2, 15);                          // Предварительно
  products: Product[] = new Array<Product>((this.temp), (this.temp1), (this.temp2));  // Жёсткий хардкод
  selectedBar = 'Products';
  cart: Product[] = [];
  check = false;

  onNavigate(feature: string) {
    this.selectedBar = feature;
  }
  productAdded(item: Product) {
    for (let i = 0; i < this.cart.length; i++) {
      if (item.name === this.cart[i].name) {
        this.cart[i].amount = item.amount;
        this.check = true;
      }
    }
    if (this.check === false) {
    this.cart.push(item);
    }
    this.check = false;
  }
  OnDeleted(index: number) {
    const temp = index;
    if (index !== this.products.length - 1) {
    for (index ; index < this.products.length - 1; index++) {
      this.products[index + 1].index = index;
    }
  }
    this.products.splice(temp, 1);
  }
  onDeleteFromCart(item: Product) {
        for (let i = 0; i < this.cart.length; i ++) {
          if (this.cart[i].index === item.index) {
            this.cart.splice(i , 1);
          }
        }
      }
      onEdited(product: Product) {
        for (let i = 0; i < this.products.length; i ++) {
          if (this.products[i].index === product.index) {
            this.products[i].name = product.name;
            this.products[i].description = product.description;
            this.products[i].price = product.price;
          }
        }
      }
}
