import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../Product.model';

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.component.html',
  styleUrls: ['./shopping.component.css']
})
export class ShoppingComponent implements OnInit {

  @Input() cart: Product[];
  @Output() onDelete = new EventEmitter<Product>();

  constructor() { }

  ngOnInit() {
  }

  calcPrice() {
    let totalPrice = 0;
    for (let i = 0; i < this.cart.length; i++) {
      totalPrice += this.cart[i].price * this.cart[i].amount;
    }
    return totalPrice;
  }
  onClickDelete(product: Product) {
    this.onDelete.emit(product);
  }
}
